<?php
use lib\route; 

require_once __DIR__.DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'route.php';


$test = new route( );

try {
    $test->setRoute( file_get_contents( "example.json" ) );    
    
    //echo $test->partDistance( 'IV4673' , 1 );
    
    echo 'IV4673:'.$test->distance( 'IV4673' )." km\n";
    echo 'Airval:'.$test->timeArrival( 'IV4673' )."\n";
    echo 'Start:'.$test->partTimeArrival( 'IV4673', 0 )."\n";
    echo 'Point 1:'.$test->partTimeArrival( 'IV4673', 1 )."\n";
    echo 'Point 2 (airval):'.$test->partTimeArrival( 'IV4673', 2 )."\n";
    echo "In air for 2016-01-07 12:10: ".implode( $test->inAir( new DateTime( '2016-01-07 12:10' )) ,',' )."\n";
    echo "In air for 2016-01-07 15:30: ".implode( $test->inAir( new DateTime( '2016-01-07 15:30' )) ,',' )."\n";
    
    
} catch ( \Exception $e ) {
        
    die( 'Something went wrong: '. $e->getMessage() );
    
}

