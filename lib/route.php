<?php
namespace lib;

class route {
    
    protected $routes = [];
    const EARTH_RADIUS = 6372795;

    static function calculateTheDistance ($φA, $λA, $φB, $λB) {
 
        // перевести координаты в радианы
        $lat1 = $φA * M_PI / 180;
        $lat2 = $φB * M_PI / 180;
        $long1 = $λA * M_PI / 180;
        $long2 = $λB * M_PI / 180;
 
        // косинусы и синусы широт и разницы долгот
        $cl1 = cos($lat1);
        $cl2 = cos($lat2);
        $sl1 = sin($lat1);
        $sl2 = sin($lat2);
        $delta = $long2 - $long1;
        $cdelta = cos($delta);
        $sdelta = sin($delta);
 
        // вычисления длины большого круга
        $y = sqrt(pow($cl2 * $sdelta, 2) + pow($cl1 * $sl2 - $sl1 * $cl2 * $cdelta, 2));
        $x = $sl1 * $sl2 + $cl1 * $cl2 * $cdelta;
 
        //
        $ad = atan2($y, $x);
        $dist = $ad * self::EARTH_RADIUS / 1000 ;
 
        return $dist;
    }
    
    public function addRoute( $key, $flight ) 
    {
        foreach ( [ 'name', 'registration', 'start', 'tr', 'speed' ] as $param ) 
        {
            if ( empty( $flight->$param ) ) throw new \Exception( "No ".$param.' for '.$key );
        }
            
        if ( \DateTime::createFromFormat( 'Y-m-d H:i', $flight->start ) === FALSE ) 
        {
            throw new \Exception( "Incorreect start date format: ".$flight->start );
        }           
            
        if ( ctype_digit( $flight->speed ) === FALSE )
        {
            throw new \Exception( "Incorreect speed value: ".$flight->speed );
        }
        
        $this->routes[ $key ] = $flight;
    }
    
    public function setRoute( $jsondata ) {

        $this->routes = [];
        
        $table = \json_decode( $jsondata );
        
        if ( ! isset( $table->routes ) ) throw new \Exception("fail to decode format");
        
        foreach ( $table->routes as $key => $flight ) 
        { // checking data
            $this->addRoute( $key, $flight );
        }
    }
    
    public function getFlight( $key ) 
    {
        if ( !isset( $this->routes[ $key ] ) ) throw new \Exception("There no such flight");
        return $this->routes[ $key ];
    }
    
    public function distance( $number ) 
    {
        $data = $this->getFlight( $number );
        
        $dist = 0;
        
        for ( $i=1; $i < count( $data->tr ); $i++ ) {
          $dist+=$this->partDistance( $number, $i );
        }
        
        return round( $dist , 1 );
    }

    public function timeArrival( $number ) 
    {
        $data = $this->getFlight( $number );
        
        $time = $this->distance( $number ) / (float)$data->speed;
        
        return date( 'Y-m-d H:i', strtotime( $data->start ) + $time * 60 * 60 );
    }
    
    public function partDistance($number, $leg ) 
    {
        $data = $this->getFlight( $number );
        
        if ( !isset( $data->tr[ $leg ] ) || !isset( $data->tr[ $leg -1 ] ) ) throw new \Exception("inccorrect leg value ".$leg );
        /*
        echo "From:\n";
        print_r( $data->tr[ $leg - 1 ] );
        echo "To:\n";
        print_r( $data->tr[ $leg  ] );
        */
        return self::calculateTheDistance( 
                        $data->tr[ $leg - 1 ][ 0 ], $data->tr[ $leg - 1 ][ 1 ],
                        $data->tr[ $leg ][ 0 ],     $data->tr[ $leg ][ 1 ] 
                    );
    }
    
    public function partTimeArrival( $number, $point ) 
    {
        $data = $this->getFlight( $number );
        
        if ( $point == 0 ) return $data->start; // point 0 == start
        
        $dist = 0;
        
        for ( $leg=1; $leg <= $point; $leg++ ) {
            $dist += $this->partDistance( $number, $leg );
        }
        
        $time = $dist / (float)$data->speed;
        
        return date( 'Y-m-d H:i', strtotime( $data->start ) + $time * 60 * 60 );
        
    }
    
    public function inAir( \DateTime $date = null) 
    {
        if ( $date == null ) $date = new \DateTime();
        
        $air = [];
        
        foreach ( $this->routes as $number => $data ) 
        {
            $start =  new \DateTime( $data->start );
            $airval = new \DateTime( $this->timeArrival( $number ) );

            if (  ( $date->getTimestamp() > $start->getTimestamp() ) && ( $date->getTimestamp() < $airval->getTimestamp() ) ) $air[] = $number;
        }
        
        return $air;
    }
 }
